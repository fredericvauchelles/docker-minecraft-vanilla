#!/bin/bash

start_server()
{
  echo "Setting permissions"
  chown -R minecraft: ${MINECRAFT_HOME}
  cp $MINECRAFT_JAR ${MINECRAFT_HOME}/server.jar

  cd ${MINECRAFT_HOME}
  echo "[EULA] Accepting EULA"
  echo "#By changing the setting below to TRUE you are indicating your agreement to our EULA \(https://account.mojang.com/documents/minecraft_eula\)." > eula.txt
  echo "eula=true" >> eula.txt

  java -server -Xms512M -Xmx9g -XX:+UseG1GC -XX:+UnlockExperimentalVMOptions -XX:MaxGCPauseMillis=100 -XX:+DisableExplicitGC -XX:TargetSurvivorRatio=90 -XX:G1NewSizePercent=50 -XX:G1MaxNewSizePercent=80 -XX:InitiatingHeapOccupancyPercent=10 -XX:G1MixedGCLiveThresholdPercent=50 -XX:+AggressiveOpts -XX:+AlwaysPreTouch  -jar server.jar nogui
}

start_server
